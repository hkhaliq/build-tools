import webbrowser, time

server='http://pm.corp.bionym.com:'
reload_brw="?reload=1"
waterfall='/waterfall'
ports=[
		'10301',
		'10302',
		'10303',
		'10304',
		'10305',
		'10306',
		'10307',
		'10309',
		'10310',
		'10311',
		'10312',
		'10313',
		'10314'
	 ]
for port in ports:
	site=server+port+waterfall+reload_brw
	session=webbrowser.open_new_tab(site)
	time.sleep(3)
